package com.xjj.mall.service.impl;

import com.google.gson.Gson;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.framework.web.support.XJJParameter;
import com.xjj.mall.common.exception.XmallException;
import com.xjj.mall.common.jedis.JedisClient;
import com.xjj.mall.common.utils.IDUtil;
import com.xjj.mall.dao.MemberDao;
import com.xjj.mall.dao.OrderDao;
import com.xjj.mall.dao.OrderItemDao;
import com.xjj.mall.dao.OrderShippingDao;
import com.xjj.mall.dao.ThanksDao;
import com.xjj.mall.entity.AddressEntity;
import com.xjj.mall.entity.MemberEntity;
import com.xjj.mall.entity.OrderEntity;
import com.xjj.mall.entity.OrderItemEntity;
import com.xjj.mall.entity.OrderShippingEntity;
import com.xjj.mall.entity.ThanksEntity;
import com.xjj.mall.pojo.CartProduct;
import com.xjj.mall.pojo.Order;
import com.xjj.mall.pojo.OrderInfo;
import com.xjj.mall.pojo.PageOrder;
import com.xjj.mall.service.OrderService;
import com.xjj.mall.util.DtoUtil;
import com.xjj.mall.util.EmailUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author zhanghejie
 */
@Service
public class OrderServiceImpl extends XjjServiceSupport<OrderEntity> implements OrderService {

    private final static Logger log= LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private MemberDao memberDao;    //用户
    @Autowired
    private OrderDao orderDao;    //订单
    @Autowired
    private OrderItemDao orderItemDao;  //订单商品
    @Autowired
    private OrderShippingDao orderShippingDao;  //订单物流
    @Autowired
    private ThanksDao thanksDao;

    //@Autowired
    //private JedisClient jedisClient;

    @Value("${CART_PRE}")
    private String CART_PRE;
    @Value("${EMAIL_SENDER}")
    private String EMAIL_SENDER;
    @Value("${PAY_EXPIRE}")
    private int PAY_EXPIRE;

    @Autowired
    private EmailUtil emailUtil;

    @Override
    public PageOrder getOrderList(Long userId, int page, int size) {

        //分页
        if(page<=0) {
            page = 1;
        }
        //PageHelper.startPage(page,size);

        PageOrder pageOrder=new PageOrder();
        List<Order> list=new ArrayList<>();

//        Tb\\OrderExample example=new TbOrderExample();
//        TbOrderExample.Criteria criteria= example.createCriteria();
//        criteria.andUserIdEqualTo(userId);
//        example.setOrderByClause("create_time DESC");
//        List<TbOrder> listOrder =tbOrderMapper.selectByExample(example);
        
        XJJParameter param = new XJJParameter();
        param.addQuery("query.userId@eq@l",userId);
        param.addOrderByDesc("createTime");
        List<OrderEntity> listOrder = orderDao.findListLimit(param.getQueryMap(), (page-1)*size, size);
        for(OrderEntity tbOrder:listOrder){

            judgeOrder(tbOrder);

            Order order=new Order();
            //orderId
            order.setOrderId(Long.valueOf(tbOrder.getOrderId()));
            //orderStatus
            order.setOrderStatus(String.valueOf(tbOrder.getStatus()));
            //createDate
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = formatter.format(tbOrder.getCreateTime());
            order.setCreateDate(date);
            //address
            OrderShippingEntity tbOrderShipping=orderShippingDao.getByColumn("order_id",tbOrder.getOrderId());
            AddressEntity address=new AddressEntity();
            address.setUserName(tbOrderShipping.getReceiverName());
            address.setStreetName(tbOrderShipping.getReceiverAddress());
            address.setTel(tbOrderShipping.getReceiverPhone());
            order.setAddressInfo(address);
            //orderTotal
            if(tbOrder.getPayment()==null){
                order.setOrderTotal(new BigDecimal(0));
            }else{
                order.setOrderTotal(tbOrder.getPayment());
            }
            //goodsList
//            TbOrderItemExample exampleItem=new TbOrderItemExample();
//            TbOrderItemExample.Criteria criteriaItem= exampleItem.createCriteria();
//            criteriaItem.andOrderIdEqualTo(tbOrder.getOrderId());
//            List<TbOrderItem> listItem =tbOrderItemMapper.selectByExample(exampleItem);
            
            param.clear();
            param.addQuery("query.orderId@eq@s",tbOrder.getOrderId());
            List<OrderItemEntity> listItem = orderItemDao.findList(param.getQueryMap());
            
            
            List<CartProduct> listProduct=new ArrayList<>();
            for(OrderItemEntity tbOrderItem:listItem){
                CartProduct cartProduct= DtoUtil.TbOrderItem2CartProduct(tbOrderItem);
                listProduct.add(cartProduct);
            }
            order.setGoodsList(listProduct);
            list.add(order);
        }
        //PageInfo<Order> pageInfo=new PageInfo<>(list);
        pageOrder.setTotal(getMemberOrderCount(userId));
        pageOrder.setData(list);
        return pageOrder;
    }

    @Override
    public Order getOrder(Long orderId) {

        Order order=new Order();

        OrderEntity tbOrder=orderDao.getByColumn("order_id", String.valueOf(orderId));
        if(tbOrder==null){
            throw new XmallException("通过id获取订单失败");
        }

        String validTime=judgeOrder(tbOrder);
        if(validTime!=null){
            order.setFinishDate(validTime);
        }

        //orderId
        order.setOrderId(Long.valueOf(tbOrder.getOrderId()));
        //orderStatus
        order.setOrderStatus(String.valueOf(tbOrder.getStatus()));
        //createDate
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String createDate = formatter.format(tbOrder.getCreateTime());
        order.setCreateDate(createDate);
        //payDate
        if(tbOrder.getPaymentTime()!=null){
            String payDate = formatter.format(tbOrder.getPaymentTime());
            order.setPayDate(payDate);
        }
        //closeDate
        if(tbOrder.getCloseTime()!=null){
            String closeDate = formatter.format(tbOrder.getCloseTime());
            order.setCloseDate(closeDate);
        }
        //finishDate
        if(tbOrder.getEndTime()!=null&&tbOrder.getStatus()==4){
            String finishDate = formatter.format(tbOrder.getEndTime());
            order.setFinishDate(finishDate);
        }
        //address
        OrderShippingEntity tbOrderShipping=orderShippingDao.getByColumn("order_id", tbOrder.getOrderId());
        AddressEntity address=new AddressEntity();
        address.setUserName(tbOrderShipping.getReceiverName());
        address.setStreetName(tbOrderShipping.getReceiverAddress());
        address.setTel(tbOrderShipping.getReceiverPhone());
        order.setAddressInfo(address);
        //orderTotal
        if(tbOrder.getPayment()==null){
            order.setOrderTotal(new BigDecimal(0));
        }else{
            order.setOrderTotal(tbOrder.getPayment());
        }
        //goodsList
//        TbOrderItemExample exampleItem=new TbOrderItemExample();
//        TbOrderItemExample.Criteria criteriaItem= exampleItem.createCriteria();
//        criteriaItem.andOrderIdEqualTo(tbOrder.getOrderId());
//        List<TbOrderItem> listItem =tbOrderItemMapper.selectByExample(exampleItem);
        
        List<OrderItemEntity> listItem = orderItemDao.findListByColumn("order_id",tbOrder.getOrderId());
        
        List<CartProduct> listProduct=new ArrayList<>();
        for(OrderItemEntity tbOrderItem:listItem){

            CartProduct cartProduct= DtoUtil.TbOrderItem2CartProduct(tbOrderItem);

            listProduct.add(cartProduct);
        }
        order.setGoodsList(listProduct);
        return order;
    }

    @Override
    public int cancelOrder(Long orderId) {

        OrderEntity tbOrder=orderDao.getByColumn("order_id", String.valueOf(orderId));
        if(tbOrder==null){
            throw new XmallException("通过id获取订单失败");
        }
        tbOrder.setStatus(5);
        tbOrder.setCloseTime(new Date());
        orderDao.update(tbOrder);
        return 1;
    }

    @Override
    public Long createOrder(OrderInfo orderInfo) {

    	MemberEntity member=memberDao.getById(orderInfo.getUserId());
        if(member==null){
            throw new XmallException("获取下单用户失败");
        }

        OrderEntity order=new OrderEntity();
        //生成订单ID
        Long orderId = IDUtil.getRandomId();
        order.setOrderId(String.valueOf(orderId));
        order.setUserId(Long.valueOf(orderInfo.getUserId()));
        order.setBuyerNick(member.getUsername());
        order.setPayment(orderInfo.getOrderTotal());
        order.setCreateTime(new Date());
        order.setUpdateTime(new Date());
        //0、未付款，1、已付款，2、未发货，3、已发货，4、交易成功，5、交易关闭，6、交易失败
        order.setStatus(0);
        orderDao.save(order);

        
        List<CartProduct> list=orderInfo.getGoodsList();
        for(CartProduct cartProduct:list){
            OrderItemEntity orderItem=new OrderItemEntity();
            //生成订单商品ID
            //Long orderItemId = IDUtil.getRandomId();
            //orderItem.setId(String.valueOf(orderItemId));
            orderItem.setItemId(String.valueOf(cartProduct.getProductId()));
            orderItem.setOrderId(String.valueOf(orderId));
            orderItem.setNum(Math.toIntExact(cartProduct.getProductNum()));
            orderItem.setPrice(cartProduct.getSalePrice());
            orderItem.setTitle(cartProduct.getProductName());
            orderItem.setPicPath(cartProduct.getProductImg());
            orderItem.setTotalFee(cartProduct.getSalePrice().multiply(BigDecimal.valueOf(cartProduct.getProductNum())));

            orderItemDao.save(orderItem);
//            //删除购物车中含该订单的商品
//            try{
//                List<String> jsonList = jedisClient.hvals(CART_PRE + ":" + orderInfo.getUserId());
//                for (String json : jsonList) {
//                    CartProduct cart = new Gson().fromJson(json,CartProduct.class);
//                    if(cart.getProductId().equals(cartProduct.getProductId())){
//                        jedisClient.hdel(CART_PRE + ":" + orderInfo.getUserId(),cart.getProductId()+"");
//                    }
//                }
//            }catch (Exception e){
//                e.printStackTrace();
//            }
        }

        //物流表
        OrderShippingEntity orderShipping=new OrderShippingEntity();
        orderShipping.setOrderId(String.valueOf(orderId));
        orderShipping.setReceiverName(orderInfo.getUserName());
        orderShipping.setReceiverAddress(orderInfo.getStreetName());
        orderShipping.setReceiverPhone(orderInfo.getTel());
        orderShipping.setCreated(new Date());
        orderShipping.setUpdated(new Date());
        orderShippingDao.save(orderShipping);

        return orderId;
    }

    @Override
    public int delOrder(Long orderId) {

    	orderDao.deleteByColumn("order_id", String.valueOf(orderId));
//        if(tbOrderMapper.deleteByPrimaryKey(String.valueOf(orderId))!=1){
//            throw new XmallException("删除订单失败");
//        }

    	
//        TbOrderItemExample example=new TbOrderItemExample();
//        TbOrderItemExample.Criteria criteria= example.createCriteria();
//        criteria.andOrderIdEqualTo(String.valueOf(orderId));
//        List<TbOrderItem> list =tbOrderItemMapper.selectByExample(example);
    	
    	//orderItemDao.findListByColumn("order_id",String.valueOf(orderId));
    	orderItemDao.deleteByColumn("order_id",String.valueOf(orderId));
//        for(TbOrderItem tbOrderItem:list){
//            if(tbOrderItemMapper.deleteByPrimaryKey(tbOrderItem.getId())!=1){
//                throw new XmallException("删除订单商品失败");
//            }
//        }
    	
    	orderShippingDao.deleteByColumn("order_id",String.valueOf(orderId));

//        if(tbOrderShippingMapper.deleteByPrimaryKey(String.valueOf(orderId))!=1){
//            throw new XmallException("删除物流失败");
//        }
        return 1;
    }

    @Override
    public int payOrder(ThanksEntity tbThanks) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time=sdf.format(new Date());
        //tbThanks.set(time);
        tbThanks.setCreateDate(new Date());
        MemberEntity tbMember = memberDao.getById(tbThanks.getUserId());
        if(tbMember!=null){
            tbThanks.setUsername(tbMember.getUsername());
        }
        
        
        thanksDao.save(tbThanks);
        
        
        //设置订单为已付款
        OrderEntity tbOrder=orderDao.getByColumn("order_id", tbThanks.getOrderId());
        tbOrder.setStatus(1);
        tbOrder.setUpdateTime(new Date());
        tbOrder.setPaymentTime(new Date());
        orderDao.update(tbOrder);
        //发送通知确认邮件
        //String tokenName= UUID.randomUUID().toString();
        //String token= UUID.randomUUID().toString();
        //设置验证token键值对 tokenName:token
        //jedisClient.set(tokenName,token);
        //jedisClient.expire(tokenName,PAY_EXPIRE);
        //emailUtil.sendEmailDealThank(EMAIL_SENDER,"【XMall商城】支付待审核处理",tokenName,token,tbThanks);
        return 1;
    }

    /**
     * 判断订单是否超时未支付
     */
    public String judgeOrder(OrderEntity tbOrder){

        String result=null;
        if(tbOrder.getStatus()==0){
            //判断是否已超1天
            long diff=System.currentTimeMillis()-tbOrder.getCreateTime().getTime();
            long days = diff / (1000 * 60 * 60 * 24);
            if(days>=1){
                //设置失效
                tbOrder.setStatus(5);
                tbOrder.setCloseTime(new Date());
                orderDao.update(tbOrder);
            }else {
                //返回到期时间
                long time=tbOrder.getCreateTime().getTime()+1000 * 60 * 60 * 24;
                result= String.valueOf(time);
            }
        }
        return result;
    }

    public int getMemberOrderCount(Long userId){

//        TbOrderExample example=new TbOrderExample();
//        TbOrderExample.Criteria criteria= example.createCriteria();
//        criteria.andUserIdEqualTo(userId);
//        List<TbOrder> listOrder =tbOrderMapper.selectByExample(example);
//        if(listOrder!=null){
//            return listOrder.size();
//        }
//        return 0;
    	
    	XJJParameter param = new XJJParameter();
        param.addQuery("query.userId@eq@l",userId);
    	int count = orderDao.getCount(param.getQueryMap());
    	
    	return count;
    }

	@Override
	public XjjDAO<OrderEntity> getDao() {
		return orderDao;
	}
    
    
    
    
}
